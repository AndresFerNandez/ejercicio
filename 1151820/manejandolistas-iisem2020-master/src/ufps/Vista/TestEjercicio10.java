/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import java.util.Scanner;
import ufps.util.coleciones_seed.Ejercicio10;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author AndresFernan
 */
public class TestEjercicio10 {
    public static void main(String[] args) throws Exception {
        Ejercicio10 e = new Ejercicio10();
        Scanner sc = new Scanner (System.in);
        System.out.println(e.validarXYpilas("OSOS&OSOS") ? "SI PERTENECE A LA FORMA X&Y" : "NO PERTENECE A LA FORMA X&Y");
        System.out.println(e.validarXYcolas("ABBDYCA&ACYDBBA") ? "SI PERTENECE A LA FORMA X&Y" : "NO PERTENECE A LA FORMA X&Y");
        System.out.println(e.validarXYvector("OSOS&OSOS") ? "SI PERTENECE A LA FORMA X&Y" : "NO PERTENECE A LA FORMA X&Y");
    }
}
