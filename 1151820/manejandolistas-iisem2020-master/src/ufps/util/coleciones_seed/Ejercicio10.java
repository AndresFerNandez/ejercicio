/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

/**
 *
 * @author AndresFernan
 */
public class Ejercicio10 {
    public boolean validarXYpilas(String cadena) throws Exception {
        if(cadena == null || cadena.isEmpty()) throw new Exception("La cadena de caracteres está vacía");
        try{
            if(!cadena.contains("&")) return false; // Si la cadena no tiene un '&' nunca se va a poder evaluar la forma X&Y
            
            Pila<Character> p1 = new Pila<>();
            Pila<Character> p2 = new Pila<>();
            Cola<Character> c1 = new Cola<>();
            
            for(int i = 0; i < cadena.length(); i++){
                p1.push(cadena.charAt(i));
            }
            
            boolean validar = false;
            while(!p1.esVacia()){
                char tmp = p1.pop();
                if(tmp == '&') validar = true;
                if(!validar && tmp != '&') p2.push(tmp);
                if(validar && tmp != '&') c1.enColar(tmp);
            }
            
            if(p2.getTamano() != c1.getTamano()) return false; // Si las estructuras no tienen el mismo tamaño no puede ser el reverso idéntico
            
            while(!p2.esVacia() && !c1.esVacia()){
                if(!p2.pop().equals(c1.deColar())) return false;
            }
        }catch(Exception e){
            e.getMessage();
        }
        return true;
    }
    
    public boolean validarXYcolas(String cadena) throws Exception{
        if(cadena == null || cadena.isEmpty()) throw new Exception("La cadena de caracteres está vacía");
        try{
            if(!cadena.contains("&")) return false; // Si la cadena no tiene un '&' nunca se va a poder evaluar la forma X&Y
            
            Cola<Character> queue = new Cola<>();
            Cola<Character> c1 = new Cola<>();
            Pila<Character> p1 = new Pila<>();
            for(int i = 0; i < cadena.length(); i++){
                queue.enColar(cadena.charAt(i));
            }
            
            boolean validar = false;
            while(!queue.esVacia()){ // Llenar las estructuras con los respectivos datos
                char tmp = queue.deColar();
                if(tmp == '&') validar = true;
                if(!validar && tmp != '&') c1.enColar(tmp);
                if(validar && tmp != '&') p1.push(tmp);
            }
            
            if(c1.getTamano() != p1.getTamano()) return false; // Si las estructuras no tienen el mismo tamaño no puede ser el reverso idéntico
            
            while(!c1.esVacia() && !p1.esVacia()){
                if(!c1.deColar().equals(p1.pop())) return false;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return true;
    }
    
    public boolean validarXYvector(String cadena) throws Exception {
        if(cadena == null || cadena.length() == 0) throw new Exception("La cadena está vacía");
        try{
            if(!cadena.contains("&")) return false; // Si la cadena no tiene un '&' nunca se va a poder evaluar la forma X&Y
            
            Character[] arr = new Character[cadena.length()];
            Cola<Character> c1 = new Cola<>();
            Cola<Character> c2 = new Cola<>();
            for(int i = 0; i < arr.length; i++){
                arr[i] = cadena.charAt(i);
            }
            
            if(cadena.contains("&")){
                int index = cadena.indexOf("&");
                /* Llenas las 2 colas con los respectivos datos */
                for(int i = 0; i < index; i++){
                    c1.enColar(arr[i]);
                }

                for(int i = arr.length - 1; i > index; i--){
                    c2.enColar(arr[i]);
                }

                if(c1.getTamano() != c2.getTamano()) return false; // Si las estructuras no tienen el mismo tamaño no puede ser el reverso idéntico
                else{
                    while(!c1.esVacia() && !c2.esVacia()){
                        if(!c1.deColar().equals(c2.deColar())) return false;
                    }
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return true;
    }
}
